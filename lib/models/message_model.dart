class MessageListModel {
  MessageListModel({
    required this.messageList,
  });
  late final List<MessageList> messageList;

  MessageListModel.fromJson(Map<String, dynamic> json){
    messageList = List.from(json['message_list']).map((e)=>MessageList.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['message_list'] = messageList.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class MessageList {
  MessageList({
    required this.userId,
    required this.userAvatar,
    required this.userNickName,
    required this.messageTime,
    required this.lastMessage,
    required this.isRead,
    required this.isTop,
    required this.unreadNum,
  });
  late final int userId;
  late final String userAvatar;
  late final String userNickName;
  late final String messageTime;
  late final String lastMessage;
  late final bool isRead;
  late final bool isTop;
  late final int unreadNum;

  MessageList.fromJson(Map<String, dynamic> json){
    userId = json['user_id'];
    userAvatar = json['user_avatar'];
    userNickName = json['user_nick_name'];
    messageTime = json['message_time'];
    lastMessage = json['last_message'];
    isRead = json['is_read'];
    isTop = json['is_top'];
    unreadNum = json['unread_num'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['user_id'] = userId;
    _data['user_avatar'] = userAvatar;
    _data['user_nick_name'] = userNickName;
    _data['message_time'] = messageTime;
    _data['last_message'] = lastMessage;
    _data['is_read'] = isRead;
    _data['is_top'] = isTop;
    _data['unread_num'] = unreadNum;
    return _data;
  }
}