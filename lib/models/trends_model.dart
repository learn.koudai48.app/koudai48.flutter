class TrendListModel {
  TrendListModel({
    required this.trendList,
  });
  late final List<TrendList> trendList;

  TrendListModel.fromJson(Map<String, dynamic> json){
    trendList = List.from(json['trend_list']).map((e)=>TrendList.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['trend_list'] = trendList.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class TrendList {
  TrendList({
    required this.userId,
    required this.userAvatar,
    required this.userNickName,
    required this.trendTime,
    required this.userLevel,
    required this.isFollow,
    required this.content,
    required this.trendImages,
    required this.zanNum,
    required this.pinlunNum,
  });
  late final int userId;
  late final String userAvatar;
  late final String userNickName;
  late final String trendTime;
  late final int userLevel;
  late final bool isFollow;
  late final String content;
  late final List<String> trendImages;
  late final int zanNum;
  late final int pinlunNum;

  TrendList.fromJson(Map<String, dynamic> json){
    userId = json['user_id'];
    userAvatar = json['user_avatar'];
    userNickName = json['user_nick_name'];
    trendTime = json['trend_time'];
    userLevel = json['user_level'];
    isFollow = json['is_follow'];
    content = json['content'];
    trendImages = List.castFrom<dynamic, String>(json['trend_images']);
    zanNum = json['zan_num'];
    pinlunNum = json['pinlun_num'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['user_id'] = userId;
    _data['user_avatar'] = userAvatar;
    _data['user_nick_name'] = userNickName;
    _data['trend_time'] = trendTime;
    _data['user_level'] = userLevel;
    _data['is_follow'] = isFollow;
    _data['content'] = content;
    _data['trend_images'] = trendImages;
    _data['zan_num'] = zanNum;
    _data['pinlun_num'] = pinlunNum;
    return _data;
  }
}