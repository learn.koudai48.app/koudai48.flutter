

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/common/iconfonts.dart';
import 'package:koudai48/theme/theme_default.dart';
import 'package:koudai48/view/tabs/tabs_controller.dart';

class TabsView extends StatelessWidget {
  TabsView({super.key});
  final _tabsController =Get.put(TabsController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
      body: PageView(
        controller: _tabsController.pageController,
        children: _tabsController.pages,
        onPageChanged: (index){
          _tabsController.switchCurrentTab(index);
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: AppColorsDefault.purple,
        unselectedItemColor: AppColorsDefault.black,
        currentIndex: _tabsController.currentTab.value,
        type: BottomNavigationBarType.fixed,
        onTap: (index){
          _tabsController.switchCurrentTab(index);
          _tabsController.pageController.jumpToPage(index);
        },
        items: const [
          BottomNavigationBarItem(icon: Icon(IconFonts.tabHome,),label: "首页",),
          BottomNavigationBarItem(icon: Icon(IconFonts.tabTrends),label: "动态",),
          BottomNavigationBarItem(icon: Icon(IconFonts.tabMessage),label: "消息",),
          BottomNavigationBarItem(icon: Icon(IconFonts.tabCenter),label: "个人",),
        ],
      ),
    ));
  }
}
