

import 'package:get/get.dart';
import 'package:koudai48/view/home/home_controller.dart';
import 'package:koudai48/view/tabs/tabs_controller.dart';

class TabsBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => TabsController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => HomeController());
  }
}