

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/view/center/center_view.dart';
import 'package:koudai48/view/home/home_view.dart';
import 'package:koudai48/view/message/message_view.dart';
import 'package:koudai48/view/trends/trends_view.dart';

class TabsController extends GetxController{
  RxInt currentTab=2.obs;
  PageController pageController=PageController(initialPage: 2);

  final List<Widget> pages=[
    const HomeView(),
    TrendsView(),
    const MessageView(),
    const CenterView(),
  ];

  void switchCurrentTab(int index){
    currentTab.value=index;
  }
}