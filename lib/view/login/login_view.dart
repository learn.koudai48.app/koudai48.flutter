import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/common/app_widget.dart';
import 'package:koudai48/common/app_unit.dart';
import 'package:koudai48/theme/theme_default.dart';
import 'package:koudai48/view/login/login_controller.dart';

class LoginView extends StatelessWidget {
  LoginView({super.key});
  final LoginController _loginController = Get.put(LoginController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          const Text(
            "登录",
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          AppWidgetSizeBox(width: AppUnit.width(20)),
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              AppWidgetSizeBox(height: AppUnit.height(50)),
              _title("Welcom", 25, AppColorsDefault.gray,),
              _title("欢迎来到口袋48", 29, AppColorsDefault.black,
                  fontWeight: FontWeight.bold),
              AppWidgetSizeBox(height: AppUnit.height(30)),
              _userInput(_loginController),
              AppWidgetSizeBox(height: AppUnit.height(15)),
              _passInput(_loginController),
              _loginBtn(),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _loginBtn() {
  return Expanded(
    child: Align(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          onPressed: () {},
          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(AppColorsDefault.black)),
          child: Text("登录",style: TextStyle(fontSize: AppUnit.fontSize(20),color: AppColorsDefault.white,letterSpacing: 2.0),),
        ),
      ),
    ),
  );
}

Widget _passInput(LoginController loginController) {
  return  Obx(
    ()=> TextField(
        obscureText: true,
        obscuringCharacter: '*',
        style: TextStyle(
          color: AppColorsDefault.gray,
          fontSize: AppUnit.fontSize(15),
          height: 1.0,
        ),
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.visiblePassword,
        decoration: const InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 9.0),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColorsDefault.gray)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColorsDefault.gray)),
          hintText: "请输入密码",
          hintStyle: TextStyle(
            color: AppColorsDefault.gray,
          ),
        ),
        controller: TextEditingController(text: loginController.pass.value),
        onChanged: (value){
          loginController.setPass(value);
        },
    ),
  );
}

Widget _userInput(LoginController loginController) {
  return Obx(
    ()=> TextField(
      style: TextStyle(
        color: Colors.black,
        fontSize: AppUnit.fontSize(15),
        height: 1.0,
      ),
      textInputAction: TextInputAction.next,
      keyboardType: TextInputType.phone,
      decoration: const InputDecoration(
        contentPadding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 9.0),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColorsDefault.gray)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColorsDefault.gray)),
        hintText: "请输入电话号码",
        hintStyle: TextStyle(
          color: AppColorsDefault.gray,
        ),
      ),
      controller: TextEditingController(text: loginController.phone.value),
      onChanged: (value){
        loginController.setPhone(value);
      },
    ),
  );
}

Widget _title(String text, num fontSize, Color color,
    {FontWeight? fontWeight}) {
  return Align(
    alignment: Alignment.centerLeft,
    child: Text(
      text,
      style: TextStyle(
        color: color,
        fontWeight: fontWeight,
        fontSize: AppUnit.fontSize(fontSize),
      ),
    ),
  );
}
