// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/common/app_widget.dart';
import 'package:koudai48/common/app_unit.dart';
import 'package:koudai48/common/iconfonts.dart';
import 'package:koudai48/models/trends_model.dart';
import 'package:koudai48/theme/theme_default.dart';
import 'package:koudai48/view/trends/trends_controller.dart';

class TrendsView extends StatelessWidget {
  TrendsView({super.key});

  final TrendsController trendsController_ = Get.put(TrendsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text("动态"),
      ),
      body: Obx(
        () => RefreshIndicator(
          onRefresh:trendsController_.pullToRefresh,
          child: ListView.separated(
            shrinkWrap: true,
            padding: const EdgeInsets.all(10),
            itemCount: trendsController_.trendList.length,
            separatorBuilder: (context, index) => const Text(""),
            // 分隔符
            itemBuilder: (BuildContext context, index) {
              return _trendItem(trendsController_.trendList[index], context,
                  trendsController_, index);
            },
          ),
        ),
      ),
    );
  }
}

num avatarWidth = 50;
double nickNameLeftPadding = 16.0;
num trendItemBodyLeftPadding = avatarWidth + nickNameLeftPadding;

Widget _trendItem(TrendList trendList, BuildContext context,
    TrendsController trendsController_, int index) {
  return Column(
    children: [
      _trendHeader(trendList),
      _trendBody(trendList),
      AppWidgetSizeBox(height: 10),
      _images(trendList),
      AppWidgetSizeBox(height: 10),
      _icons(trendList, context, trendsController_),
      Padding(
        padding: EdgeInsets.only(left: trendItemBodyLeftPadding as double),
        child: TextField(

          focusNode: trendsController_.textFieldFocusNodes[index],
          controller: trendsController_.textFieldControllers[index],
          decoration: const InputDecoration(
            isDense: true,
            filled: true,
            fillColor: Color.fromARGB(255, 242, 242, 242),
            contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 13.0),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColorsDefault.gray,)),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColorsDefault.gray,)),
            labelText: "输入评论",
            labelStyle: TextStyle(
              color: AppColorsDefault.gray,
            )
          ),
          style: const TextStyle(
            fontSize: 10
          ),
          onTap: () {
            trendsController_.showKeyboardWithAboveInput(
                context, trendsController_.textFieldFocusNodes[index]);
          },
        ),
      )
    ],
  );
}

// ---------------

void _showCommentDialog(BuildContext context, FocusNode focusNode) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("添加评论"),
        content: TextField(
          focusNode: focusNode,
          decoration: InputDecoration(labelText: "输入评论"),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: Text("取消"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          ElevatedButton(
            child: Text("提交"),
            onPressed: () {
              // 处理提交评论的逻辑
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Widget _trendHeader(TrendList trendList_) {
  return Row(
    children: [
      _avatar(trendList_.userAvatar),
      Expanded(
        flex: 4,
        child: Padding(
          padding: EdgeInsets.only(left: nickNameLeftPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(trendList_.userNickName),
                  AppWidgetSizeBox(width: AppUnit.width(5)),
                  const Icon(
                    IconFonts.xingbienan,
                    size: 12,
                    color: AppColorsDefault.blue,
                  ),
                  AppWidgetSizeBox(width: AppUnit.width(5)),
                  Text(
                    "Lv.${trendList_.userLevel}",
                    style: TextStyle(
                        fontSize: AppUnit.fontSize(10),
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              Text(
                trendList_.trendTime,
                style: TextStyle(
                    color: AppColorsDefault.gray,
                    fontSize: AppUnit.fontSize(10)),
              ),
            ],
          ),
        ),
      ),
      Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: const Color.fromARGB(255, 247, 247, 247)),
        ),
        child: trendList_.isFollow
            ? Text(
                "已关注",
                style: TextStyle(
                    fontSize: AppUnit.fontSize(13),
                    color: AppColorsDefault.gray),
              )
            : Text(
                "+关注",
                style: TextStyle(
                    fontSize: AppUnit.fontSize(13),
                    color: AppColorsDefault.black),
              ),
      ),
    ],
  );
}

Widget _trendBody(TrendList trendList_) {
  return Align(
    alignment: Alignment.centerLeft,
    child: Container(
      padding: EdgeInsets.only(
        left: AppUnit.width(trendItemBodyLeftPadding),
      ),
      child: Text(
        trendList_.content,
        maxLines: 5,
        overflow: TextOverflow.ellipsis,
      ),
    ),
  );
}

Widget _images(TrendList trendList_) {
  return GridView.builder(
    physics: const NeverScrollableScrollPhysics(),
    // 禁用滚动
    shrinkWrap: true,
    itemCount:
        trendList_.trendImages.length > 6 ? 6 : trendList_.trendImages.length,
    padding: EdgeInsets.only(left: trendItemBodyLeftPadding as double),
    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 3,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      childAspectRatio: 1,
    ),
    itemBuilder: (ctx, index) {
      return _trendImages(trendList_.trendImages[index]);
    },
  );
}

Widget _icons(TrendList trendList_, BuildContext context,
    TrendsController trendsController_) {
  Widget _text_(String text) {
    return Text(
      text,
      style: const TextStyle(color: AppColorsDefault.gray),
    );
  }

  return Container(
    height: 25,
    padding: EdgeInsets.only(left: trendItemBodyLeftPadding as double),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
            flex: 1,
            child: Row(
              children: [
                const Icon(IconFonts.dianzan_no,
                    size: 19, color: AppColorsDefault.gray),
                Align(
                  alignment: Alignment.bottomRight,
                  child: _text_(trendList_.zanNum.toString()),
                )
              ],
            )),
        Expanded(
            flex: 1,
            child: Row(
              children: [
                const Icon(IconFonts.pinglun,
                    size: 19, color: AppColorsDefault.gray),
                Align(
                  alignment: Alignment.bottomRight,
                  child: _text_(trendList_.pinlunNum.toString()),
                )
              ],
            )),
        const Expanded(
          flex: 1,
          child: Icon(
            IconFonts.fenxiang,
            size: 19,
            color: AppColorsDefault.gray,
          ),
        ),
        const Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerRight,
              child:
                  Icon(IconFonts.more, size: 19, color: AppColorsDefault.gray),
            )),
      ],
    ),
  );
}

Widget _avatar(String avatarUrl) {
  return Container(
    clipBehavior: Clip.hardEdge,
    decoration: const BoxDecoration(
      shape: BoxShape.circle,
    ),
    width: AppUnit.width(avatarWidth),
    height: AppUnit.height(avatarWidth),
    child: Image.network(
      avatarUrl,
      fit: BoxFit.cover,
    ),
  );
}

Widget _trendImages(String url) {
  return Container(
    clipBehavior: Clip.hardEdge,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(5),
    ),
    width: AppUnit.width(avatarWidth),
    height: AppUnit.height(avatarWidth),
    child: Image.network(
      url,
      fit: BoxFit.cover,
    ),
  );
}
