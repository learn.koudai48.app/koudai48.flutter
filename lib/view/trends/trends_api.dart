
import 'package:koudai48/common/app_http.dart';
import 'package:koudai48/config/http_config.dart';
import 'package:koudai48/models/trends_model.dart';

Future<List<TrendList>> getTrendListApi()async{
  final response=await AppHttp.get(HttpManage.trends_list);
  final TrendListModel result=TrendListModel.fromJson(response);
  return result.trendList;
}