

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/models/trends_model.dart';
import 'package:koudai48/view/trends/trends_api.dart';

class TrendsController extends GetxController{
  RxList<TrendList> trendList=<TrendList>[].obs;
  late List<FocusNode> textFieldFocusNodes  = [];//FocusNode()
  late List<TextEditingController> textFieldControllers = [];// TextEditingController()

  @override
  void onInit() {
    super.onInit();
    getTrendList();
  }

  void showKeyboardWithAboveInput(BuildContext context,FocusNode focusNode) {
    Future.delayed(Duration(milliseconds: 100), () {
      FocusScope.of(context).requestFocus(focusNode);
    });
  }

  void getTrendList()async {
    final List<TrendList> result=await getTrendListApi();
    textFieldControllers = List.generate(
      result.length,
          (index) => TextEditingController(),
    );
    textFieldFocusNodes = List.generate(
      result.length,
          (index) => FocusNode(),
    );
    trendList.value=result;
    update();
  }

  Future<void> pullToRefresh() async{
    getTrendList();
  }

  @override
  void onClose() {
    textFieldFocusNodes.forEach((controller) {
      controller.dispose();
    });
    textFieldFocusNodes.clear();
    textFieldControllers.forEach((controller) {
      controller.dispose();
    });
    textFieldControllers.clear();
    super.onClose();
  }


}