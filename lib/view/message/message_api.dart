


import 'package:koudai48/common/app_http.dart';
import 'package:koudai48/config/http_config.dart';
import 'package:koudai48/models/message_model.dart';

Future<List<MessageList>> messageListApi()async{
  final response=await AppHttp.get(HttpManage.message_list);
  final MessageListModel result=MessageListModel.fromJson(response);
  return result.messageList;
}