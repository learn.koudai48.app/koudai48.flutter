import 'package:flutter/material.dart';
import 'package:koudai48/common/app_unit.dart';
import 'package:koudai48/common/app_widget.dart';
import 'package:koudai48/models/message_model.dart';
import 'package:koudai48/theme/theme_default.dart';

class KoudaiView extends StatelessWidget {
  final List<MessageList> messageList;
  final Future<void> Function() refreshHandle;
  const KoudaiView(this.messageList,this.refreshHandle, {super.key});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refreshHandle,
      child: ListView.separated(
          padding: const EdgeInsets.all(10),
          itemBuilder: (context, index) {
            return _messageCardItem(messageList[index]);
          },
          separatorBuilder: (_, i) => const Text(""),
          itemCount: messageList.length),
    );
  }
}

Widget _messageCardItem(MessageList messageListItem) {
  return SizedBox(
    height: AppUnit.width(50),
    child: Row(
      children: [
        Container(
          clipBehavior: Clip.hardEdge,
          width: AppUnit.width(50),
          height: double.infinity,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Image.network(
            messageListItem.userAvatar,
            fit: BoxFit.cover,
          ),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      messageListItem.userNickName,
                      style: TextStyle(
                        overflow: TextOverflow.ellipsis,
                        fontWeight: FontWeight.bold,
                        fontSize: AppUnit.fontSize(16),
                      ),
                    )),
                Row(
                  children: [
                    messageListItem.unreadNum>0?
                    Container(
                      width: AppUnit.width(26),
                      height: AppUnit.height(19),
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColorsDefault.red,
                      ),
                      child: Center(child: Text(messageListItem.unreadNum.toString(),style: const TextStyle(color: Colors.white),)),
                    ):const SizedBox(),
                    AppWidgetSizeBox(width: 6),
                    Expanded(
                      child: Text(
                        messageListItem.lastMessage,
                        style: const TextStyle(
                          overflow: TextOverflow.ellipsis,
                          color: AppColorsDefault.gray,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    ),
  );
}
