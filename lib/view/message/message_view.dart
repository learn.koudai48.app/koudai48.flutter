import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:koudai48/common/app_unit.dart';
import 'package:koudai48/view/message/message_controller.dart';
import 'package:koudai48/view/message/modules/fanpai_view.dart';
import 'package:koudai48/view/message/modules/koudai_view.dart';

class MessageView extends StatefulWidget {
  const MessageView({super.key});

  @override
  State<MessageView> createState() => _MessageViewState();
}

class _MessageViewState extends State<MessageView> with SingleTickerProviderStateMixin {

  late TabController _tabController;
  final MessageController messageController_=Get.put(MessageController());

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("消息"),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(20.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: TabBar(
              isScrollable: true, // 将Tab标签靠左对齐
              controller: _tabController,
              tabs:  [
                Tab(child: Text("口袋",style: TextStyle(fontWeight: FontWeight.bold,fontSize: AppUnit.fontSize(20)),),),
                Tab(child: Text("翻牌",style: TextStyle(fontWeight: FontWeight.bold,fontSize: AppUnit.fontSize(20)),),),
              ],
            ),
          ),
        ),
      ),
      body: Obx(
        ()=> TabBarView(
          controller: _tabController,
          children: [
            KoudaiView(messageController_.messageList.value,messageController_.refreshPage),
            FanpaiView()
          ],
        ),
      ),
    );
  }
}

