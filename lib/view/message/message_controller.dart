


import 'package:get/get.dart';
import 'package:koudai48/models/message_model.dart';
import 'package:koudai48/view/message/message_api.dart';

class MessageController extends GetxController {

  RxList<MessageList> messageList =<MessageList>[].obs;

  @override
  void onInit() {
    super.onInit();
    getMessageList();
  }

  void getMessageList()async{
    final List<MessageList> result=await messageListApi();
    messageList.value=result;
    update();
  }

  Future<void> refreshPage()async{
    getMessageList();
  }
}