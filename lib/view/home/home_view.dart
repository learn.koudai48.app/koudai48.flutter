import 'package:flutter/material.dart';
import 'package:koudai48/common/app_images.dart';
import 'package:koudai48/common/app_widget.dart';
import 'package:koudai48/common/app_unit.dart';
import 'package:koudai48/theme/theme_default.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("首页"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            _searchInput(),
            AppWidgetSizeBox(height: AppUnit.height(30)),
            _swiper(),
            AppWidgetSizeBox(height: AppUnit.height(30)),
            _horizontalMenu(),
          ],
        ),
      ),
    );
  }
}

Widget _horizontalMenu() {
  return SizedBox(
    height: AppUnit.height(100),
    child: Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          flex: 2,
          child: Column(
            children: [
              Image.asset(
                AppImages.gongyan,
                fit: BoxFit.cover,
              ),
              Text("公演",style: TextStyle(color:AppColorsDefault.purpleText,fontSize: AppUnit.fontSize(16),),),
            ],
          ),
        ),
        const Spacer(flex: 1),
        Expanded(
          flex: 2,
          child: Column(
            children: [
              Image.asset(
                AppImages.chengyuan,
                fit: BoxFit.cover,
              ),
              Text("成员",style: TextStyle(color:AppColorsDefault.purpleText,fontSize: AppUnit.fontSize(16),),),
            ],
          ),
        ),
        const Spacer(flex: 1),
        Expanded(
          flex: 2,
          child: Column(
            children: [
              Image.asset(
                AppImages.mengpiao,
                fit: BoxFit.cover,
              ),
              Text("门票",style: TextStyle(color:AppColorsDefault.purpleText,fontSize: AppUnit.fontSize(16),),),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget _swiper() {
  return Container(
    clipBehavior: Clip.hardEdge,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(16),
    ),
    width: double.infinity,
    height: 200,
    child: Image.asset(
      AppImages.swiper01,
      fit: BoxFit.cover,
    ),
  );
}

Widget _searchInput() {
  return TextField(
    decoration: InputDecoration(
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10.0)),
      contentPadding:
          const EdgeInsets.symmetric(vertical: 1.0, horizontal: 9.0),
      hintText: 'Search Something Here',
      hintStyle: const TextStyle(color: AppColorsDefault.gray),
      border: const OutlineInputBorder(),
      filled: true,
      fillColor: Colors.grey[200],
      prefixIcon: const Icon(
        Icons.search,
        color: AppColorsDefault.gray,
      ),
    ),
    onChanged: (value) {
      // 在这里执行搜索逻辑
    },
  );
}
