




import 'package:get/get.dart';
import 'package:koudai48/router/routes.dart';
import 'package:koudai48/view/login/login_view.dart';
import 'package:koudai48/view/tabs/tabs_binding.dart';
import 'package:koudai48/view/tabs/tabs_view.dart';

// ignore_for_file: constant_identifier_names

class AppPages{
  AppPages._();
  static const INITROUTE =Routes.TABS;

  static final routes=[
    GetPage(name: Routes.LOGIN, page: ()=>LoginView()),
    GetPage(name: Routes.TABS, page: ()=>TabsView(),binding:TabsBinding() )
  ];
}