
import 'package:dio/dio.dart';
import 'package:koudai48/config/http_config.dart';

class AppHttp {
  AppHttp._();
  static final Dio dio=Dio(
    BaseOptions(
       baseUrl: HttpManage.base_url,
      connectTimeout: const Duration(seconds: 3000),
      receiveTimeout: const Duration(seconds: 3000),
      responseType: ResponseType.json,
    )
  );

  // GET
  static Future<dynamic> get(String apiUrl,{bool isAuth=true})async{
    try{
      Response response;
      if(isAuth){
        // var token= await LocalStorage.get(AppInfo.tokenLocalKey);
        // print(token);
        response = await dio.get(apiUrl,options: Options(
            // headers: {
            //   "Authorization": "Bearer $token",
            // }
        ));
      }else{
        response = await dio.get(apiUrl);
      }
      return response.data;
    }catch(e){
      print("请求超时");
      return null;
    }
  }
}