

// ignore_for_file: constant_identifier_names

import 'package:flutter/cupertino.dart';



class IconFonts {
  static const IconData tabHome=IconData(
  0xe77e,
  fontFamily: "iconfonts",
  matchTextDirection: true,
  );
  static const IconData tabTrends=IconData(
    0xe6e8,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData tabMessage=IconData(
    0xe8bd,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData tabCenter=IconData(
    0xe61e,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData more=IconData(
    0xe60b,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData dianzan_ok=IconData(
    0xe600,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData pinglun=IconData(
    0xe62c,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData dianzan_no=IconData(
    0xe611,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData fenxiang=IconData(
    0xe601,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData xingbienan=IconData(
    0xe63a,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
  static const IconData xingbienv=IconData(
    0xe60e,
    fontFamily: "iconfonts",
    matchTextDirection: true,
  );
}

