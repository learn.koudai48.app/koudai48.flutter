






import 'dart:ui';

import 'package:flutter/material.dart';

class AppColorsDefault {
  static const Color black=Colors.black;
  static const Color red=Color.fromARGB(255, 255, 95, 124);
  static const Color purple=Color.fromARGB(255, 153, 97, 250);
  static const Color purpleText=Color.fromARGB(255, 140, 129, 159);
  static const Color gray=Color.fromARGB(255, 187, 187, 187);
  static const Color blue=Color.fromARGB(255, 106, 192, 236);
  static const Color white=Colors.white;
}